# Document database in Go
A rudimentary document database in Go. Supports storing JSON documents, and retrieving them using search filters and indices

## Try it out

## Acknowledgement
This is inspired by the work at [](https://notes.eatonphil.com/documentdb.html)
