package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/dgraph-io/badger/v3"
	"github.com/rs/xid"
	"gitlab.com/anihm136_learning_projects/document-db/pkg/query"
	"gitlab.com/anihm136_learning_projects/document-db/pkg/query/queryparser"
)

func (s server) addDocument(w http.ResponseWriter, r *http.Request) {
	dec := json.NewDecoder(r.Body)
	var document map[string]interface{}
	err := dec.Decode(&document)
	if err != nil {
		jsonResponse(w, nil, err)
		return
	}

	id := xid.New().String()
	docBytes, err := json.Marshal(document)
	if err != nil {
		jsonResponse(w, nil, err)
		return
	}

	s.index.Index(id, document)

	err = s.db.Update(func(txn *badger.Txn) error {
		err := txn.Set([]byte(id), docBytes)
		return err
	})
	if err != nil {
		jsonResponse(w, nil, err)
		return
	}

	jsonResponse(w, map[string]interface{}{
		"id": id,
	}, nil)
}

func makeLookupPath(p *queryparser.QueryPart) string {
	pathParts := make([]string, 0)
	for _, part := range p.Path {
		part.Data = strings.TrimFunc(part.Data, func(r rune) bool {
			return r == '"'
		})
		pathParts = append(pathParts, part.Data)
	}

	pathString := strings.Join(pathParts, ".")
	p.Value.Data = strings.TrimFunc(p.Value.Data, func(r rune) bool {
		return r == '"'
	})
	return fmt.Sprintf("%s=%v", pathString, p.Value.Data)
}

func (s server) searchDocuments(w http.ResponseWriter, r *http.Request) {
	queryString := r.URL.Query().Get("q")
	q, err := queryparser.ParseQuery(queryString)
	if err != nil {
		jsonResponse(w, nil, err)
		return
	}

	var matchingDocs []map[string]interface{}

	isRange := false
	idsArgumentCount := map[string]int{}
	nonRangeArguments := 0
	for _, part := range q.QueryParts {
		if part.Operator == ":" {
			nonRangeArguments++

			ids, err := s.lookup(makeLookupPath(part))
			if err != nil {
				jsonResponse(w, nil, err)
				return
			}

			for _, id := range ids {
				idsArgumentCount[id]++
			}
		} else {
			isRange = true
		}
	}

	var idsInAll []string
	for id, count := range idsArgumentCount {
		if count == nonRangeArguments {
			idsInAll = append(idsInAll, id)
		}
	}

	fmt.Println(r.URL.Query().Get("skipIndex"))
	if r.URL.Query().Get("skipIndex") == "true" {
		idsInAll = nil
	}

	if len(idsInAll) > 0 {
		for _, id := range idsInAll {
			document, err := s.getDocumentByID([]byte(id))
			if err != nil {
				jsonResponse(w, nil, err)
				return
			}

			match, err := query.Match(document, queryString)
			if err != nil {
				jsonResponse(w, nil, err)
				return
			}
			if !isRange || match {
				matchingDocs = append(matchingDocs, map[string]interface{}{
					"id":   id,
					"body": document,
				})
			}
		}
	} else {
		fmt.Println("We're doing this")
		err := s.db.View(func(txn *badger.Txn) error {
			opts := badger.DefaultIteratorOptions
			opts.PrefetchSize = 10

			it := txn.NewIterator(opts)
			defer it.Close()
			for it.Rewind(); it.Valid(); it.Next() {
				item := it.Item()
				k := item.Key()

				err := item.Value(func(val []byte) error {
					var doc map[string]interface{}
					err := json.Unmarshal(val, &doc)
					if err != nil {
						return err
					}

					match, err := query.Match(doc, queryString)
					if err != nil {
						return err
					}
					if match {
						var data map[string]interface{}
						err := json.Unmarshal(val, &data)
						if err != nil {
							return err
						}

						matchingDocs = append(matchingDocs, map[string]interface{}{
							"id":   string(k),
							"data": data,
						})
					}
					return nil
				})
				if err != nil {
					return err
				}
			}

			return nil
		})

		if err != nil {
			jsonResponse(w, nil, err)
			return
		}
	}
	jsonResponse(
		w,
		map[string]interface{}{
			"documents": matchingDocs,
			"count":     len(matchingDocs),
		},
		nil,
	)
}

func (s server) getDocument(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get(":id")
	log.Printf("ID: %s\n", id)

	doc, err := s.getDocumentByID([]byte(id))
	if err != nil {
		jsonResponse(w, nil, err)
		return
	}

	jsonResponse(w, map[string]interface{}{
		"document": doc,
	}, nil)
}
