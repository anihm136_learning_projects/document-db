package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/dgraph-io/badger/v3"
	"gitlab.com/anihm136_learning_projects/document-db/pkg/index"
)

type server struct {
	port  string
	db    *badger.DB
	index index.IndexDb
}

func newServer(dbPath, port string) (*server, error) {
	s := server{db: nil, port: "8080"}
	var err error
	s.db, err = badger.Open(badger.DefaultOptions(dbPath))
	if err != nil {
		return nil, err
	}
	i, err := badger.Open(badger.DefaultOptions(fmt.Sprintf("%s.index", dbPath)))
	if err != nil {
		return nil, err
	}

	s.index.DB = i
	return &s, err
}

func main() {
	s, err := newServer("doc.db", "8080")
	if err != nil {
		log.Fatal(err)
	}

	// err = s.reindex()
	// if err != nil {
	// 	log.Fatal(err)
	// }

	router := s.createRouter()

	log.Println("Listening on port " + s.port)
	log.Fatal(http.ListenAndServe(":"+s.port, router))
}
