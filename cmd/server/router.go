package main

import (
	"net/http"

	"github.com/bmizerany/pat"
)

func (s server) createRouter() http.Handler {
	router := pat.New()
	
	router.Post("/docs", http.HandlerFunc(s.addDocument))
	router.Get("/docs", http.HandlerFunc(s.searchDocuments))
	router.Get("/docs/:id", http.HandlerFunc(s.getDocument))
	
	return logRequest(router)
}
