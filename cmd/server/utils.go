package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"runtime/debug"
	"strings"

	"github.com/dgraph-io/badger/v3"
)

type response struct {
	Status string                 `json:"status"`
	Body   map[string]interface{} `json:"body"`
	Error  string                 `json:"error"`
}

func jsonResponse(w http.ResponseWriter, body map[string]interface{}, err error) {
	resp := response{
		Status: "ok",
		Body:   body,
	}

	if err != nil {
		resp.Status = "error"
		resp.Error = err.Error()
		w.WriteHeader(http.StatusBadRequest)
	} else {
		w.WriteHeader(http.StatusOK)
	}

	buf := new(bytes.Buffer)
	enc := json.NewEncoder(buf)
	err = enc.Encode(resp)
	if err != nil {
		serverError(w, err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, err = buf.WriteTo(w)
	if err != nil {
		serverError(w, err)
	}
}

func serverError(w http.ResponseWriter, err error) {
	trace := fmt.Sprintf("%s\n%s", err.Error(), debug.Stack())
	logErr := log.Output(2, trace)
	if logErr != nil {
		log.Printf("could not show trace: %v\n", logErr)
	}

	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

func (s server) getDocumentByID(id []byte) (map[string]interface{}, error) {
	var docBytes []byte
	err := s.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get(id)
		if err != nil {
			return err
		}

		err = item.Value(func(val []byte) error {
			docBytes = append(docBytes, val...)
			return nil
		})
		return err
	})
	if err != nil {
		return nil, err
	}

	var doc map[string]interface{}
	err = json.Unmarshal(docBytes, &doc)
	return doc, err
}

func (s server) reindex() error {
	err := s.db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10

		it := txn.NewIterator(opts)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			k := item.Key()

			err := item.Value(func(val []byte) error {
				var doc map[string]interface{}
				err := json.Unmarshal(val, &doc)
				if err != nil {
					return err
				}

				s.index.Index(string(k), doc)
				return nil
			})
			if err != nil {
				return err
			}
		}

		return nil
	})
	return err
}

func (s server) lookup(pathValue string) ([]string, error) {
	idsString, err := s.index.Lookup(pathValue)
	if err != nil {
		return nil, fmt.Errorf("could not look up pathvalue [%#v]: %s", pathValue, err)
	}

	return strings.Split(string(idsString), ","), nil
}
