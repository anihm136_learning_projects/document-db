package index

import (
	"errors"
	"fmt"
	"log"
	"strings"

	"github.com/dgraph-io/badger/v3"
)

type IndexDb struct {
	*badger.DB
}

func getPathValues(obj map[string]interface{}, prefix string) []string {
	var pvs []string
	for key, val := range obj {
		switch t := val.(type) {
		case map[string]interface{}:
			pvs = append(pvs, getPathValues(t, key)...)
			continue
		case []interface{}:
			// Can't handle arrays
			continue
		default:
			if prefix != "" {
				key = prefix + "." + key
			}

			pvs = append(pvs, fmt.Sprintf("%s=%v", key, val))
		}
	}

	return pvs
}

func (i IndexDb) Index(id string, document map[string]interface{}) {
	pv := getPathValues(document, "")

	for _, pathValue := range pv {
		var docID string
		err := i.View(func(txn *badger.Txn) error {
			item, err := txn.Get([]byte(pathValue))
			if err != nil {
				return err
			}

			err = item.Value(func(val []byte) error {
				docID = string(val)
				return nil
			})
			return err
		})

		if err != nil && !errors.Is(err, badger.ErrKeyNotFound) {
			log.Printf("Error in retrieving path: %v", err)
			return
		}

		if len(docID) == 0 {
			docID = id
		} else {
			ids := strings.Split(docID, ",")

			found := false
			for _, existingId := range ids {
				if id == existingId {
					found = true
					break
				}
			}

			if !found {
				docID = docID + "," + id
			}
		}

		err = i.Update(func(txn *badger.Txn) error {
			err := txn.Set([]byte(pathValue), []byte(docID))
			return err
		})
		if err != nil {
			log.Printf("Could not update index: %v", err)
		}
	}
}

func (i IndexDb) Lookup(path string) (string, error) {
	var docID string
	err := i.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(path))
		if err != nil {
			return err
		}

		err = item.Value(func(val []byte) error {
			docID = string(val)
			return nil
		})
		return err
	})

	if errors.Is(err, badger.ErrKeyNotFound) {
		return "", nil
	}
	return docID, err
}
