package query

import (
	"fmt"
	"strconv"

	"gitlab.com/anihm136_learning_projects/document-db/pkg/query/queryparser"
)

func Match(doc map[string]interface{}, query string) (bool, error) {
	q, err := queryparser.ParseQuery(query)
	if err != nil {
		return false, err
	}

	for _, queryPart := range q.QueryParts {
		data, ok := getPath(doc, queryPart.Path)
		if !ok {
			return false, nil
		}

		// Check for equality
		if queryPart.Operator == ":" {
			if fmt.Sprintf("%v", data) != queryPart.Value.Data {
				return false, nil
			}
			continue
		}

		// Check greater/lesser
		rhs, err := strconv.ParseFloat(queryPart.Value.Data, 64)
		if err != nil {
			return false, nil
		}

		var lhs float64
		switch t := data.(type) {
		case float64:
			lhs = t
		case float32:
		case uint:
		case uint8:
		case uint16:
		case uint32:
		case uint64:
		case int:
		case int8:
		case int16:
		case int32:
		case int64:
			lhs = float64(t)
		case string:
			lhs, err = strconv.ParseFloat(t, 64)
			if err != nil {
				return false, nil
			}
		default:
			return false, nil
		}

		if queryPart.Operator == ":<" {
			if !(lhs < rhs) {
				return false, nil
			}
		} else {
			if !(lhs > rhs) {
				return false, nil
			}
		}
	}

	return true, nil
}

func getPath(doc map[string]interface{}, path []*queryparser.Element) (interface{}, bool) {
	var segment interface{} = doc
	for _, pathComponent := range path {
		doc, ok := segment.(map[string]interface{})
		if !ok {
			return nil, false
		}
		segment, ok = doc[pathComponent.Data]
		if !ok {
			return nil, false
		}
	}
	return segment, true
}
