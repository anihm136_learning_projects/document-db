package queryparser

import "fmt"

type invalidStringError struct {
	str     string
	message string
}

func (i invalidStringError) Error() string {
	return fmt.Sprintf("cannot parse %q: %s", i.str, i.message)
}

