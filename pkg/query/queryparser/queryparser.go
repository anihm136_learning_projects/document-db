package queryparser

import (
	"github.com/alecthomas/participle/v2"
)

type Query struct {
	QueryParts []*QueryPart `parser:"@@*"`
}

type QueryPart struct {
	Path     []*Element `parser:"(@@ '.'?)*"`
	Operator string     `parser:"@(':' ('>' | '<')?)"`
	Value    *Element   `parser:"@@"`
}

type Element struct {
	Data string `parser:"@Ident | @String | @Int"`
}

func ParseQuery(input string) (*Query, error) {
	parser, err := participle.Build(&Query{})
	if err != nil {
		return &Query{}, err
	}

	q := &Query{}
	err = parser.ParseString("", input, q)
	if err != nil {
		return &Query{}, err
	}

	return q, nil
}
