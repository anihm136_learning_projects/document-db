package queryparser

import (
	"errors"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func checkError(t *testing.T, expected, actual error) {
	t.Helper()

	switch expected.(type) {
	case nil:
		if actual != nil {
			t.Fatalf("expected %v, got %v", expected, actual)
		}
	case invalidStringError:
		if _, ok := actual.(invalidStringError); !ok {
			t.Fatalf("expected %v, got %v", expected, actual)
		}
	default:
		if !errors.Is(actual, expected) {
			t.Fatalf("expected %v, got %v", expected, actual)
		}
	}
}

func TestParse(t *testing.T) {
	tests := []struct {
		name       string
		input      string
		isJSONPath bool
		output     *Query
		err        error
	}{
		{
			name:  "should parse JSON path",
			input: `a.b.c.d:1 "a.b"."c.d":>2`,
			output: &Query{
				QueryParts: []*QueryPart{
					{
						Path: []*Element{
							{Data: "a"},
							{Data: "b"},
							{Data: "c"},
							{Data: "d"},
						},
						Operator: ":",
						Value:    &Element{Data: "1"},
					},
					{
						Path: []*Element{
							{Data: `"a.b"`},
							{Data: `"c.d"`},
						},
						Operator: ":>",
						Value:    &Element{Data: "2"},
					},
				},
			},
			err: nil,
		},
		{
			name:  "should parse empty string",
			input: "",
			output: &Query{},
			err: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q, err := ParseQuery(tt.input)
			checkError(t, tt.err, err)

			if !cmp.Equal(q, tt.output) {
				t.Errorf("difference between expected and actual output: %v", cmp.Diff(tt.output, q))
			}
		})
	}
}
